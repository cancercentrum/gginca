#' Graphical representation of variable coverage per year
#' 
#' @param data a data.frame
#' @param year quoted name of date och year variable in \code{data} to use 
#' for cross tabulation.
#' @return Object of class 'gg' and 'ggplot'.
#' 
#' @examples 
#' 
#' # Get some data
#' set.seed(12345)
#' iris2 <- as.matrix(iris)
#' iris2[sample(length(iris2), 100)] <- NA
#' iris2 <- as.data.frame(iris2)
#' iris2$year <- sample(2010:2015, nrow(iris2), replace = TRUE)
#' 
#' # The graph
#' ggcoverage(iris2, "year")
#' 
#' @export
ggcoverage <- function(data, year){

    stopifnot(
        is.data.frame(data), 
        is.character(year), 
        length(year) == 1
    )
    
    year <- rccdates::as.year(data[[year]])
    
    df <- 
        data %>%
        group_by(year) %>%
        summarise_each(funs(round(mean(!is.na(.)) * 100))) %>%  
        tidyr::gather(variable, coverage, -year)
    
     ggplot(df, aes(y = as.factor(year), x = variable, fill = coverage)) + 
        geom_tile(aes(fill = coverage)) + 
        scale_fill_gradient(low = "white", high = "blue") + 
        xlab("Variable") + 
        ylab("Year") +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) 
}



