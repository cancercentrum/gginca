
context("rcc_color")

test_that("misc", {
  expect_error(rcc_color(-1))
  expect_equal(rcc_color(1), "#00b3f6")
  expect_equal(rcc_color(3), c("#00b3f6", "#ffb117", "#434348"))
})




test_that("rcc_rainbow works as intended", {
    expect_identical(rcc_rainbow(), rcc_rainbow(1))
    expect_equivalent(rcc_rainbow(3), c("#00FCEE", "#01F5EF", "#000017"))
    expect_warning(rcc_rainbow(16), "It is recommended to use no more than 15 colors!")
    expect_error(rcc_rainbow(-1), "n must be a positive scalar integer!")
})
